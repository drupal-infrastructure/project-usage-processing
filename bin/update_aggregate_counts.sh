#!/bin/bash
set -eux

weektimestamp=${1-}
if [ -z "$weektimestamp" ]; then weektimestamp=$(date '--date=last Sunday -1 weeks' +%s); fi


#Get the S3 bucket name to copy results to
S3_BUCKET_NAME=`echo ${S3_DATA_BUCKET_ARN} | awk -F ':::' ' { print $2 } '`

#clear out existing data - script reruns do not accumluate data
mkdir -p /data/stats/updatestats/counts
mkdir -p /data/stats/updatestats/tmpdir 
rm -rf /data/stats/updatestats/counts/$weektimestamp.*

# Handle project/release usage counts

sortargs='-f -S 3G'

# Sort and uniq the project/release usage data per week (eliminates extra calls from the same site key.)
{ time sort ${sortargs} -T /data/stats/updatestats/tmpdir -u /data/stats/updatestats/reformatted/$weektimestamp/projects/*.formatted > /data/stats/updatestats/counts/$weektimestamp.project_counts.uniq.sorted <(/bin/gzip -dc /data/stats/updatestats/reformatted/$weektimestamp/projects/*.formatted.gz); } 2>&1

# Find the number of unique sites asking us for data that week
{ time cut -f1 -d"|" /data/stats/updatestats/counts/$weektimestamp.project_counts.uniq.sorted |uniq -i |wc -l > /data/stats/updatestats/counts/$weektimestamp.uniquesitekeys ; } 2>&1
# Count the release uses
{ time cut -f2,3,4 -d"|" /data/stats/updatestats/counts/$weektimestamp.project_counts.uniq.sorted |sort ${sortargs} -T /data/stats/updatestats/tmpdir |uniq -c -i |sort -n > /data/stats/updatestats/counts/$weektimestamp.releasecounts ; } 2>&1
# Count the project uses
{ time cut -f2,4 -d"|" /data/stats/updatestats/counts/$weektimestamp.project_counts.uniq.sorted |sort ${sortargs} -T /data/stats/updatestats/tmpdir |uniq -c -i |sort -n > /data/stats/updatestats/counts/$weektimestamp.projectapicounts ; } 2>&1

# Handle submodules usage counts

# Sort and uniq the submodule usage data per week (eliminates extra calls from the same site key.)
{ time sort ${sortargs} -T /data/stats/updatestats/tmpdir -u /data/stats/updatestats/reformatted/$weektimestamp/submodules/*.formatted > /data/stats/updatestats/counts/$weektimestamp.submodule_counts.uniq.sorted ; } 2>&1
# Aggregate and count the project/release/submodule name
{ time cut -f2,3,4,5 -d"|" /data/stats/updatestats/counts/$weektimestamp.submodule_counts.uniq.sorted |sort ${sortargs} -T /data/stats/updatestats/tmpdir |uniq -c -i |sort -n > /data/stats/updatestats/counts/$weektimestamp.submodule_release_counts; } 2>&1
# Aggregate and count the project/submodule name
{ time cut -f2,4,5 -d"|" /data/stats/updatestats/counts/$weektimestamp.submodule_counts.uniq.sorted |sort ${sortargs} -T /data/stats/updatestats/tmpdir |uniq -c -i |sort -n > /data/stats/updatestats/counts/$weektimestamp.submodule_project_counts; } 2>&1

# Handle keyless project use counts

# Sort and uniq the keyless project usage data per week (eliminates extra calls from the same ip address.)
{ time sort ${sortargs} -T /data/stats/updatestats/tmpdir -u /data/stats/updatestats/reformatted/$weektimestamp/keyless/*.formatted > /data/stats/updatestats/counts/$weektimestamp.keyless_counts.uniq.sorted ; } 2>&1
{ time cut -f2,3 -d"|" /data/stats/updatestats/counts/$weektimestamp.keyless_counts.uniq.sorted |sort ${sortargs} -T /data/stats/updatestats/tmpdir |uniq -c -i |sort -n > /data/stats/updatestats/counts/$weektimestamp.keyless_project_counts; } 2>&1

# Save results for Drupal to import.
aws s3 cp '/data/stats/updatestats/counts' "s3://${S3_BUCKET_NAME}/counts/" --recursive --include "${weektimestamp}*" --exclude '*.uniq.sorted'

# Remove all but the 3 most recent reformatted directories.
find /data/stats/updatestats/reformatted -mindepth 1 -maxdepth 1 -type d -print0 | sort -z | head -z -n -3 | xargs --null rm -rv
