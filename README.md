* Set up with read-only access to `s3://drupal-fastly-log`
* Persistent mount at `/data/stats/updatestats`
* Files from `/data/stats/updatestats/counts` will be copied to a separate S3 bucket
* Daily cron `H 6 * * *` `/bin/daily_usage_processor.sh`
  Optional argument YYYY-MM-DD to catch up or repeat processing
* Weekly cron `H 2 * * 1` `bin/update_aggregate_counts.sh`
  Optional argument timestamp to catch up or repeat processing
