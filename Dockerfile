FROM debian:stable-slim

RUN apt-get update && apt-get install -y awscli && apt-get clean && rm -r /var/lib/apt/lists/*

COPY bin /bin

#Set a temp home because we run as no user
ENV HOME=/tmp

ENTRYPOINT ["/bin/daily_usage_processor.sh"]
